package ru.bakhtiyarov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public class TaskClearByProjectNameListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;
    
    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "tasks-clear-by-project-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks by project name.";
    }

    @EventListener(condition = "@taskClearByProjectNameListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CLEAR TASKS]");
        System.out.println("ENTER PROJECT NAME:");
        final String projectName = TerminalUtil.nextLine();
        @NotNull SessionDTO session = sessionService.getSession();
        taskEndpoint.removeAllByUserIdAndProjectName(session, projectName);
        System.out.println("[OK]");
    }

}