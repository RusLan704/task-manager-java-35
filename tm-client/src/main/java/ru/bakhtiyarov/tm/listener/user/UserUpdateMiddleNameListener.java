package ru.bakhtiyarov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.UserDTO;
import ru.bakhtiyarov.tm.endpoint.UserEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class UserUpdateMiddleNameListener extends AbstractListener {

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.USER_UPDATE_MIDDLE_NAME;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user middle name.";
    }

    @EventListener(condition = "@userUpdateMiddleNameListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[UPDATE MIDDLE NAME]");
        @NotNull SessionDTO session = sessionService.getSession();
        final UserDTO user = userEndpoint.findUserById(session);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @Nullable final UserDTO userUpdated = userEndpoint.updateUserMiddleName(session, middleName);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}