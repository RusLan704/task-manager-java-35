package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.listener.AbstractListener;

import java.util.List;

public interface ICommandRepository {

    @NotNull
    List<AbstractListener> getCommandList();

}

