<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../include/_header.jsp"/>
<h1>PROJECT LIST</h1>


<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">NAME</th>
        <th width="200">DESCRIPTION</th>
        <th width="100" nowrap="nowrap" align="center">EDIT</th>
        <th width="100" nowrap="nowrap" align="center">DELETE</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td align="center">
                <a href="/projects/update/${project.id}/">EDIT</a>
            </td>
            <td align="center">
                <a href="/projects/remove/${project.id}/">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="/projects/create" style="margin-top: 20px;">
    <button>CREATE PROJECT</button>
</form>
<jsp:include page="../include/_footer.jsp"/>
