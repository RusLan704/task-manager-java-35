package ru.bakhtiyarov.tm.service.converter;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.bakhtiyarov.tm.api.service.converter.ITaskConverter;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Task;

@Service
public class TaskConverter implements ITaskConverter {

    @Nullable
    @Override
    public TaskDTO toDTO(@Nullable final Task task) {
        if (task == null) return null;
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setUserId(task.getUser().getId());
        return taskDTO;
    }

    @Nullable
    @Override
    public Task toEntity(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return null;
        Task task = new Task();
        task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        return task;
    }

}
