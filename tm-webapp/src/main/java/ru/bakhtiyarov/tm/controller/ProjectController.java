package ru.bakhtiyarov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.entity.Project;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    @Autowired
    public ProjectController(
            @NotNull final IProjectService projectService
    ) {
        this.projectService = projectService;
    }

    @GetMapping("/list")
    public ModelAndView list() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-list");
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-create");
        modelAndView.addObject("project", new Project());
        return modelAndView;
    }

    @PostMapping("/create")
    public String create(@ModelAttribute("project") Project project) {
        projectService.save(project);
        return "redirect:/projects/list";
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @PathVariable(value = "id") @NotNull final String id
    ) {
        projectService.removeOneById(id);
        return new ModelAndView("redirect:/projects/list");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-update");
        @Nullable final Project project = projectService.findById(id);
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public String update(
            @ModelAttribute("project") Project project
    ) {
        projectService.updateProjectById(project.getId(), project.getName(), project.getDescription());
        return "redirect:/projects/list";
    }
    
}