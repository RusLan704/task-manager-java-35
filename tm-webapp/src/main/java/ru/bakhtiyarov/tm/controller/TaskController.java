package ru.bakhtiyarov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.entity.Task;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    @Autowired
    public TaskController(@NotNull final ITaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/list")
    public ModelAndView list() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-list");
        modelAndView.addObject("tasks", taskService.findAll());
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-create");
        modelAndView.addObject("task", new Task());
        return modelAndView;
    }

    @PostMapping("/create")
    public String create(@ModelAttribute("task") Task task) {
        taskService.save(task);
        return "redirect:/tasks/list";
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @PathVariable(value = "id") @NotNull final String id
    ) throws Exception {
        taskService.removeOneById(id);
        return new ModelAndView("redirect:/tasks/list");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-update");
        @Nullable final Task task = taskService.findById(id);
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public String update(
            @ModelAttribute("task") Task task
    ) {
        taskService.updateTaskById(task.getId(), task.getName(), task.getDescription());
        return "redirect:/tasks/list";
    }

}