package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    List<Project> deleteAllByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String name);

    @NotNull
    List<Project> findAllByUserId(@NotNull final String userId);

    @Nullable
    Project findOneByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Project findByUserIdAndName(@NotNull final String userId, @NotNull final String name);

}
