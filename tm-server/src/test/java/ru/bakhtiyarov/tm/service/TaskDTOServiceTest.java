package ru.bakhtiyarov.tm.service;

import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class TaskDTOServiceTest {

/*    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Test
    public void testCreateUserIdName() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.create("1", "name");
        Assert.assertEquals(1, taskService.findAll().size());
        TaskDTO taskDTO = taskService.findOneByName("1", "name");
        Assert.assertNotNull(taskDTO);
        Assert.assertEquals(taskDTO.getName(), "name");
        Assert.assertEquals(taskDTO.getUserId(), "1");
    }

    @Test
    public void testCreateUserIdNameDescription() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.create("1", "name", "234");
        Assert.assertEquals(1, taskService.findAll().size());
        TaskDTO taskDTO = taskService.findOneByName("1", "name");
        Assert.assertNotNull(taskDTO);
        Assert.assertEquals("name", taskDTO.getName());
        Assert.assertEquals("234", taskDTO.getDescription());
        Assert.assertEquals("1", taskDTO.getUserId());
    }

    @Test
    public void testAdd() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        TaskDTO taskDTO = new TaskDTO("1234", "2");
        taskService.add(taskDTO.getUserId(), taskDTO);
        Assert.assertEquals(1, taskService.findAll().size());
    }

    @Test
    public void testRemove() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        TaskDTO taskDTO = new TaskDTO("1234", "2");
        taskService.add(taskDTO.getUserId(), taskDTO);
        Assert.assertEquals(1, taskService.findAll().size());
        taskService.remove(taskDTO.getUserId(), taskDTO);
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    public void testFindAll() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        TaskDTO taskDTO = new TaskDTO("1234", "2");
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.add(taskDTO.getUserId(), taskDTO);
        Assert.assertEquals(4, taskService.findAll(taskDTO.getUserId()).size());
    }

    @Test
    public void clearAll() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        TaskDTO taskDTO = new TaskDTO("1234", "2");
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.add(taskDTO.getUserId(), taskDTO);
        Assert.assertEquals(4, taskService.findAll(taskDTO.getUserId()).size());
        taskService.clearAll();
        Assert.assertTrue(taskService.findAll(taskDTO.getUserId()).isEmpty());
    }

    @Test
    public void testFindOneByName() {
        TaskDTO taskDTO = new TaskDTO("task", "2231");
        taskService.add(taskDTO.getUserId(), taskDTO);
        TaskDTO tempTaskDTO = taskService.findOneByName(taskDTO.getUserId(), taskDTO.getName());
        Assert.assertNotNull(tempTaskDTO);
        Assert.assertEquals(taskDTO.getId(), tempTaskDTO.getId());
    }

    @Test
    public void testFindOneByIndex() {
        TaskDTO taskDTO = new TaskDTO("task", "2231");
        taskService.add(taskDTO.getUserId(), taskDTO);
        TaskDTO tempTaskDTO = taskService.findOneByIndex(taskDTO.getUserId(), 0);
        Assert.assertNotNull(tempTaskDTO);
        Assert.assertEquals(taskDTO.getId(), tempTaskDTO.getId());
    }

    @Test
    public void testFindOneById() {
        TaskDTO taskDTO = new TaskDTO("task", "2231");
        taskService.add(taskDTO.getUserId(), taskDTO);
        TaskDTO tempTaskDTO = taskService.findOneById(taskDTO.getUserId(), taskDTO.getId());
        Assert.assertNotNull(tempTaskDTO);
        Assert.assertEquals(taskDTO.getId(), tempTaskDTO.getId());
    }

    @Test
    public void testRemoveOneByIndex() {
        TaskDTO taskDTO = new TaskDTO("task", "2231");
        taskService.add(taskDTO.getUserId(), taskDTO);
        Assert.assertEquals(1, taskService.findAll(taskDTO.getUserId()).size());
        taskService.removeOneByIndex(taskDTO.getUserId(), 0);
        Assert.assertEquals(0, taskService.findAll(taskDTO.getUserId()).size());
    }

    @Test
    public void testRemoveOneById() {
        TaskDTO taskDTO = new TaskDTO("task", "2231");
        taskService.add(taskDTO.getUserId(), taskDTO);
        Assert.assertEquals(1, taskService.findAll(taskDTO.getUserId()).size());
        taskService.removeOneById(taskDTO.getUserId(), taskDTO.getId());
        Assert.assertEquals(0, taskService.findAll(taskDTO.getUserId()).size());
    }

    @Test
    public void testRemoveOneByName() {
        TaskDTO taskDTO = new TaskDTO("task", "2231");
        taskService.add(taskDTO.getUserId(), taskDTO);
        Assert.assertEquals(1, taskService.findAll(taskDTO.getUserId()).size());
        taskService.removeOneByName(taskDTO.getUserId(), taskDTO.getName());
        Assert.assertEquals(0, taskService.findAll(taskDTO.getUserId()).size());
    }

    @Test
    public void testUpdateTakByIndex() {
        TaskDTO taskDTO = new TaskDTO("name", "description", "123");
        taskService.add(taskDTO.getUserId(), taskDTO);
        Assert.assertEquals(1, taskService.findAll(taskDTO.getUserId()).size());
        TaskDTO tempTaskDTO = taskService.updateTaskByIndex(taskDTO.getUserId(), 0, "new name", "new description");
        Assert.assertNotNull(tempTaskDTO);
        Assert.assertEquals(tempTaskDTO.getId(), taskDTO.getId());
        Assert.assertEquals(tempTaskDTO.getName(), "new name");
        Assert.assertEquals(tempTaskDTO.getDescription(), "new description");
    }

    @Test
    public void testUpdateTaskById() {
        TaskDTO taskDTO = new TaskDTO("name", "description", "123");
        taskService.add(taskDTO.getUserId(), taskDTO);
        Assert.assertEquals(1, taskService.findAll(taskDTO.getUserId()).size());
        TaskDTO tempTaskDTO = taskService.updateTaskById(taskDTO.getUserId(), taskDTO.getId(), "new name", "new description");
        Assert.assertNotNull(tempTaskDTO);
        Assert.assertEquals(tempTaskDTO.getId(), taskDTO.getId());
        Assert.assertEquals(tempTaskDTO.getName(), "new name");
        Assert.assertEquals(tempTaskDTO.getDescription(), "new description");
        TaskDTO nullTaskDTO = taskService.updateTaskById("1234", taskDTO.getId(), "new name", "new description");
        Assert.assertNull(nullTaskDTO);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithEmptyUserId() {
        taskService.create("", "name");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithNullUserId() {
        taskService.create(null, "name");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithEmptyName() {
        taskService.create("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithNullName() {
        taskService.create("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateDescWithEmptyUserId() {
        taskService.create("", "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateDescWithNullUserId() {
        taskService.create(null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateDescWithEmptyName() {
        taskService.create("123", "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateDescWithNullName() {
        taskService.create("123", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateDescWithEmptyDesc() {
        taskService.create("123", "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateDescWithNullDesc() {
        taskService.create("123", "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeAddWithEmptyUserId() {
        taskService.add("", new TaskDTO());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeAddWithNullUserId() {
        taskService.add(null, new TaskDTO());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithEmptyUserId() {
        taskService.remove("", new TaskDTO());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithNullUserId() {
        taskService.remove(null, new TaskDTO());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllWithEmptyUserId() {
        taskService.findAll("");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllWithNullUserId() {
        taskService.findAll(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllWithEmptyUserId() {
        taskService.clear("");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllWithNullUserId() {
        taskService.clear(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithEmptyUserId() {
        taskService.removeOneByIndex("", 0);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithNullUserId() {
        taskService.removeOneByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithMinusIndex() {
        taskService.removeOneByIndex("123", -10);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithNullIndex() {
        taskService.removeOneByIndex("123", null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithIncorrectIndex() {
        TaskDTO taskDTO = new TaskDTO("name", "123");
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.removeOneByIndex("123", 2);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithEmptyUserId() {
        taskService.removeOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithNullUserId() {
        taskService.removeOneById(null, "123");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithEmptyId() {
        taskService.removeOneById("123", "");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithNullId() {
        taskService.removeOneById("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneNameIdWithEmptyUserId() {
        taskService.removeOneByName("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByNameWithNullUserId() {
        taskService.removeOneByName(null, "123");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneNameIdWithEmptyName() {
        taskService.removeOneByName("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneByNameWithNullName() {
        taskService.removeOneByName("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateTaskByIdIdWithEmptyUserId() {
        taskService.updateTaskById("", "123", "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateTaskByIdWithNullUserId() {
        taskService.updateTaskById(null, "123", "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateTaskByIdIdWithEmptyId() {
        taskService.updateTaskById("123", "", "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateTaskByIdWithNullId() {
        taskService.updateTaskById("123", null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateTaskByIdIdWithEmptyName() {
        taskService.updateTaskById("123", "123", "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateTaskByIdWithNullName() {
        taskService.updateTaskById("123", "123", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateTaskByIdIdWithEmptyDesc() {
        taskService.updateTaskById("123", "123", "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateTaskByIdWithNullDesc() {
        taskService.updateTaskById("123", "123", "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateTaskByIndexIdWithEmptyUserId() {
        taskService.updateTaskByIndex("", 0, "name", "description");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateTaskByIndexWithNullUserId() {
        taskService.updateTaskByIndex(null, 0, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateTaskByIndexIdWithMinusId() {
        taskService.updateTaskByIndex("123", -10, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateTaskByIndexWithNullId() {
        taskService.updateTaskByIndex("123", null, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateTaskByIndexWithIncorrectId() {
        TaskDTO taskDTO = new TaskDTO("name", "123");
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.updateTaskByIndex("123", 2, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateTaskByIndexIdWithEmptyName() {
        taskService.updateTaskByIndex("123", 0, "", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateTaskByIndexWithNullName() {
        taskService.updateTaskByIndex("123", 0, null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateTaskByIndexIdWithEmptyDesc() {
        taskService.updateTaskByIndex("123", 0, "name", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateTaskByIndexWithNullDesc() {
        taskService.updateTaskByIndex("123", 0, "name", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIdIdWithEmptyUserId() {
        taskService.findOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIdIdWithNullUserId() {
        taskService.findOneById(null, "123");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneIdIdWithEmptyId() {
        taskService.findOneById("123", "");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneIdIdWithNullId() {
        taskService.findOneById("123", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIndexIdWithEmptyUserId() {
        taskService.findOneById("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneIndexIdWithNullUserId() {
        taskService.findOneById(null, "123");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithMinusUserIndex() {
        taskService.findOneByIndex("123", -10);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithNullIndex() {
        taskService.findOneByIndex("123", null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneIndexIdWithIncorrectIndex() {
        TaskDTO taskDTO = new TaskDTO("name", "123");
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.add(taskDTO.getUserId(), taskDTO);
        taskService.findOneByIndex("123", 2);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameWithEmptyUserId() {
        taskService.findOneByName("", "123");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameIdWithNullUserId() {
        taskService.findOneByName(null, "123");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameWithEmptyName() {
        taskService.findOneByName("123", "");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameIdWithNullName() {
        taskService.findOneByName("123", null);
    }
*/
}