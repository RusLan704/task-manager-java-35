package ru.bakhtiyarov.tm.repository;

import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class TaskDTORepositoryTest {

  /*  @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private TaskDTO taskDTO;

    @Before
    public void addTask() {
        taskDTO = new TaskDTO("task", "description", "1");
        taskRepository.add(taskDTO);
    }

    @After
    public void removeTask() {
        taskRepository.clearAll();
    }

    @Test
    public void testAdd() {
        TaskDTO addedTaskDTO = taskRepository.findOneById(taskDTO.getUserId(), taskDTO.getId());
        Assert.assertNotNull(addedTaskDTO);
        Assert.assertEquals(taskDTO.getName(), addedTaskDTO.getName());
        Assert.assertEquals(taskDTO.getDescription(), addedTaskDTO.getDescription());
        Assert.assertEquals(taskDTO.getUserId(), addedTaskDTO.getUserId());
    }

    @Test
    public void testClear() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.add(taskDTO);
        taskRepository.add(taskDTO);
        taskRepository.add(taskDTO);
        taskRepository.clear(taskDTO.getUserId());
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.remove(taskDTO.getUserId(), taskDTO);
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        TaskDTO taskDTOTest = taskRepository.removeOneByIndex(ConstTest.FALSE_USER_ID, 0);
        Assert.assertNull(taskDTOTest);
        taskRepository.removeOneByIndex(taskDTO.getUserId(), 0);
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void testRemoveByName() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        Assert.assertNull(taskRepository.removeOneByName(ConstTest.FALSE_USER_ID, taskDTO.getName()));
        Assert.assertNull(taskRepository.removeOneByName(taskDTO.getUserId(), ConstTest.FALSE_NAME));
        taskRepository.removeOneByName(taskDTO.getUserId(), taskDTO.getName());
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.removeOneById(taskDTO.getUserId(), taskDTO.getId());
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.add(taskDTO);
        taskRepository.add(taskDTO);
        taskRepository.add(taskDTO);
        taskRepository.add(taskDTO);
        final List<TaskDTO> taskDTOS = taskRepository.findAll(taskDTO.getUserId());
        Assert.assertNotNull(taskDTOS);
        Assert.assertEquals(5, taskRepository.findAll().size());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        TaskDTO taskDTOTest = taskRepository.findOneByIndex(taskDTO.getUserId(), 0);
        Assert.assertNotNull(taskDTOTest);
        Assert.assertEquals(taskDTO.getName(), taskDTOTest.getName());
        Assert.assertEquals(taskDTO.getUserId(), taskDTOTest.getUserId());
        TaskDTO nullTaskDTO = taskRepository.findOneByIndex(taskDTO.getUserId(), 10);
        Assert.assertNull(nullTaskDTO);
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        TaskDTO taskDTOTest = taskRepository.findOneById(taskDTO.getUserId(), taskDTO.getId());
        Assert.assertNotNull(taskDTOTest);
        Assert.assertEquals(taskDTO.getUserId(), taskDTOTest.getUserId());
        Assert.assertEquals(taskDTO.getName(), taskDTOTest.getName());
        TaskDTO nullTaskDTO = taskRepository.findOneById(taskDTO.getUserId(), ConstTest.FALSE_ID);
        Assert.assertNull(nullTaskDTO);
    }

    @Test
    public void findOneByName() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        TaskDTO taskDTOTest = taskRepository.findOneByName(taskDTO.getUserId(), taskDTO.getName());
        Assert.assertNotNull(taskDTOTest);
        Assert.assertEquals(taskDTO.getUserId(), taskDTOTest.getUserId());
        Assert.assertEquals(taskDTO.getName(), taskDTOTest.getName());
        TaskDTO nullTaskDTO = taskRepository.findOneById(taskDTO.getUserId(), ConstTest.FALSE_NAME);
        Assert.assertNull(nullTaskDTO);
    }
*/
}
