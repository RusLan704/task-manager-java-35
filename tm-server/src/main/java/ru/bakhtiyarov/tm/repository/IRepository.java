package ru.bakhtiyarov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

public interface IRepository<E extends AbstractEntity> extends JpaRepository<E, String> {
}