package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    SessionDTO openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    );

    @WebMethod
    void closeSession(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    );

    @WebMethod
    void closeSessionAll(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    );

    @WebMethod
    Integer getServerPort(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    );


}
